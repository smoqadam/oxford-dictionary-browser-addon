## Oxford Dictionary browser addon

The best way to learn English is using the English to English dictionaries such as Oxford or Miriam Webster. With this extension you can check the definition of the selected word right inside the browser. 
It using the Oxford dictionary and gives you the definition, phonetic, examples and synonyms of a word. 

[Download for Firefox](https://addons.mozilla.org/en-US/firefox/addon/oxford-dictionary-addon)

[Download for Chrome](https://chrome.google.com/webstore/detail/english-to-english-dictio/aggmgejbkkjpgapfhbigiceaeaabfnpo)

![screenshot](https://user-images.githubusercontent.com/1223848/57301498-c712fc80-710b-11e9-8a92-bcad9fce1c2e.jpg)


## Contributing
If you find a bug or need a feature don't hesitate to tell me about it. It's open-source, so, if you are a developer or you can help me to improve it (develop, documentation, design, etc.), open an issue or send a PR.


